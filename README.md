# Brain Simulator

Simulation of neurons in a brain.

## Overview

This project aims to simulate the neural network of a human brain
by allowing the user to manually create, share, learn about and
use neural networks.

Neurons can be placed in the synapses of the brain and connected to
one another. When an action potential is sent to the neural network,
it will be passed down the chain of neurons to produce some output.

There will be a premade neural network to interpret a drawn numeral
into an actual number, simulating several areas of the brain's visual
cortex.
